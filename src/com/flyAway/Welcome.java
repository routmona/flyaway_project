package com.flyAway;

import javax.ws.rs.GET;
import javax.ws.rs.Path;

@Path("/")
public class Welcome {
	
	@GET
	public String welcome() {
		return "welcome to FlyAway(An Airline Booking Portal)";
	}

}
