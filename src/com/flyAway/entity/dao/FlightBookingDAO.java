package com.flyAway.entity.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

import com.flyAway.entity.FlightBooking;
import com.flyAway.entity.util.SessionUtil;

public class FlightBookingDAO {
	Configuration configuration=new Configuration().configure();
	StandardServiceRegistryBuilder builder=new StandardServiceRegistryBuilder().applySettings(configuration.getProperties());
	SessionFactory factory=configuration.buildSessionFactory(builder.build());
	
	public FlightBooking addFlightBooking(FlightBooking bean){
		Session session = factory.openSession();
	    Transaction tx = session.beginTransaction();
	    addFlightBooking(session,bean);
	    tx.commit();
	    session.close();
	    return bean;
	    
	}
	
	private FlightBooking addFlightBooking(Session session,FlightBooking bean){
		FlightBooking flightbooking = new FlightBooking();
		flightbooking.setId(bean.getId());
		flightbooking.setPassenger(bean.getPassenger());
		session.save(flightbooking);
		return null;
	}
	
	public List<FlightBooking>getFlightBookings(){
		Session session= factory.openSession();
		Transaction tx = session.beginTransaction();
		Query query = session.createQuery("from FlightBooking");
		List<FlightBooking> flightbookings= query.list();
		tx.commit();
		session.close();
		return flightbookings;
	}
	
	public int deleteFlightBooking(int id){
		Session session = factory.openSession();
		Transaction tx = session.beginTransaction();
		String hql ="delete from FlightBooking where id = :id";
		Query query = session.createQuery(hql);
		query.setInteger("id",id);
		int rowCount = query.executeUpdate();
		System.out.println("Rows affected: " + rowCount);
		tx.commit();
		session.close();
		return rowCount;
	}
	
	public int updateFlightBooking(int id,FlightBooking flightbooking){
		if(id<=0)
			return 0;
		Session session = factory.openSession();
		Transaction tx =session.beginTransaction();
		String hql = "update Flight set id = :id, passenger=:passenger where id = :id";
		Query query = session.createQuery(hql);
		
		query.setInteger("id",id);
		query.setString("id",flightbooking.getId());
		query.setString("passenger",flightbooking.getPassenger());
		
		int rowCount = query.executeUpdate();
		System.out.println("Rows affected: " + rowCount);
		tx.commit();
		session.close();
		return rowCount;
		
	}
	

}
