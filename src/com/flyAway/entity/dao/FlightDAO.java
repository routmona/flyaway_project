package com.flyAway.entity.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

import com.flyAway.entity.Flight;
import com.flyAway.entity.util.SessionUtil;

public class FlightDAO {
	
	Configuration configuration=new Configuration().configure();
	StandardServiceRegistryBuilder builder=new StandardServiceRegistryBuilder().applySettings(configuration.getProperties());
	SessionFactory factory=configuration.buildSessionFactory(builder.build());
	
	public Flight addFlight(Flight bean) {
		Session session =factory.openSession();
		Transaction tx = session.beginTransaction();
		addFlight(session,bean);
		tx.commit();
		session.close();
		return bean;
	}
	
	private Flight addFlight(Session session,Flight bean){
		Flight flight =new Flight();
		flight.setId(bean.getId());
		flight.setDeparture(bean.getDeparture());
		flight.setArrival(bean.getArrival());
		flight.setDepartureDate(bean.getDepartureDate());
		flight.setArrivalDate(bean.getArrivalDate());
		session.save(flight);
		return null;
	}
	
	public List<Flight> getFlights(){
		Session session= factory.openSession();
		Transaction tx= session.beginTransaction();
		Query query = session.createQuery("from Flight");
		List<Flight> flights =query.list();
		tx.commit();
		session.close();
		return flights;
	}
	
	public int deleteFlight(int id) {
		Session session = factory.openSession();
		Transaction tx= session.beginTransaction();
		String hql = "delete from Flight where id = :id";
		Query query = session.createQuery(hql);
		query.setInteger("id",id);
		int rowCount = query.executeUpdate();
		System.out.println("Rows affected: " + rowCount);
		tx.commit();
		session.close();
		return rowCount;
	}
	
	public int updateFlight(int id,Flight flight){
		if(id<=0)
			return 0;
		Session session =factory.openSession();
		Transaction tx = session.beginTransaction();
		String hql ="update Flight set id =:id, departure =:departure, arrival =:arrival depatureDate =:departureDate, arrivalDate =:arrivalDate where id =:id";
		Query query =session.createQuery(hql);
		query.setInteger("id",id);
		query.setString("id",flight.getId());
		query.setString("departure",flight.getDeparture());
		query.setString("arrival",flight.getArrival());
		query.setDate("departureDate",flight.getDepartureDate());
		query.setDate("arrivalDate",flight.getArrivalDate());
		int rowCount = query.executeUpdate();
		System.out.println("Rows affected: " + rowCount);
		tx.commit();
		session.close();
		return rowCount;
	}	
	}



