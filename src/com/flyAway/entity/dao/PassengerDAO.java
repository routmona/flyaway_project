package com.flyAway.entity.dao;

import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

import com.flyAway.entity.Passenger;
import com.flyAway.entity.util.SessionUtil;

public class PassengerDAO {
	Configuration configuration=new Configuration().configure();
	StandardServiceRegistryBuilder builder=new StandardServiceRegistryBuilder().applySettings(configuration.getProperties());
	SessionFactory factory=configuration.buildSessionFactory(builder.build());
	
	public void addPassenger(Passenger bean){
		Session session =factory.openSession();
		Transaction tx = session.beginTransaction();
		addPassenger(session,bean);
		tx.commit();
		session.close();
		
	}
	
	public Passenger addPassenger(Session session,Passenger bean){
		Passenger passenger = new Passenger();
		passenger.setEmail(bean.getEmail());
		passenger.setFirstName(bean.getFirstName());
		passenger.setId(bean.getId());
		passenger.setLastName(bean.getLastName());
		passenger.setPassword(bean.getPassword());
		session.save(passenger);
		return null;
	}
	
	public List<Passenger>getPassengers(){
		Session session = factory.openSession();
		Transaction tx = session.beginTransaction();
		Query query = session.createQuery("from Passenger");
		List<Passenger> passengers =query.list();
		tx.commit();
		session.close();
		return passengers;
	}
	
	public Passenger getPassengerByEmail(Passenger passenger){
		Session session = factory.openSession();
        Transaction tx = session.beginTransaction();
		Query query = session.createQuery("from Passenger where email:=email");
	    query.setParameter("email",passenger.getEmail());
		Passenger passengers= (Passenger)query.list().get(0);
		tx.commit();
		session.close();
		return passengers;
	}
	
	public int deletePassenger(int id){
		Session session = factory.openSession();
		Transaction tx = session.beginTransaction();
		String hql ="delete from Passenger where id = :id";
		Query query = session.createQuery(hql);
		query.setInteger("id",id);
		int rowCount = query.executeUpdate();
		System.out.println("Rows affected: " +rowCount);
		tx.commit();
		session.close();
		return rowCount;
	}
	
	public int updatePassenger(int id,Passenger passenger){
		if(id <=0)
		return 0;
		Session session = SessionUtil.getSession();
		Transaction tx = session.beginTransaction();
		String hql = "update Passenger set email =:email , firstName =:firstName, lastName =:lastName, password =:password where id =:id";
		Query query = session.createQuery(hql);
		
		query.setInteger("id",id);
		query.setString("email",passenger.getEmail());
		query.setString("firstName",passenger.getFirstName());
		query.setString("lastName",passenger.getLastName());
		query.setString("password",passenger.getPassword());
		int rowCount = query.executeUpdate();
		System.out.println("Rows affected: " + rowCount);
		tx.commit();
		session.close();
		return rowCount;
	}		
	}

